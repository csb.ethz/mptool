.. mptool documentation master file, created by
   sphinx-quickstart on Wed Mar 13 00:02:33 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

mptool
======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   quickstart
   api

.. Indices and tables
=====================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

.. highlight:: python
.. default-role:: obj

Introduction
============

This is the documentation for `mptool <https://gitlab.com/csb.ethz/mptool>`, a
Python module for enumeration and sampling of minimal pathways (MPs) in
metabolic networks and subnetworks.

MPs are minimal sets of reactions that need to be active (have non-zero flux)
in a metabolic (sub)network to satisfy all constraints on the network as a
whole [1]. They can also be defined as the set of support-minimal flux patterns
from elementary flux vectors (EFVs) [2].

An MP can be found by direct minimization of a mixed-integer linear program
(MILP) or by iterative minimization of multiple linear programs (LPs).
Enumeration of MPs is implemented using both of these approaches. In the
iterative case, it is done by computing minimal cuts (MCs) in a separate binary
integer program (BIP) [3]. For iterative minimization, enumeration can be
accelerated by using a graph defined by the known MPs to predict unknown MPs,
or it can be randomized to allow sampling of MPs when complete enumeration is
infeasible.

Installation
============

To install `mptool`, run the following command in your terminal:

.. code-block:: bash

   pip install mptool

Ensure you have Python (≥3.7.0) and the Gurobi optimizer (≥9.0.1) installed
before you proceed.

Quickstart
==========

Here's a simple example of how to use `mptool`:

.. code-block:: python

   import mptool as mpt

   # Load a COBRA model (e_coli_core)
   model = mpt.load_cobra_model('e_coli_core.xml')

   # Set minimal growth rate requirement
   model.reactions.BIOMASS_Ecoli_core_w_GAM.lower_bound = 0.1

   # Choose boundary reactions as subset for MP enumeration
   subset = model.boundary

   # Enumerate all MPs (and MCSs) using the iterative method with graph
   mps, mcs, complete = mpt.find_mps(model, subset=subset, method='iterative',
                                     graph=True, verbose=True)

This will enumerate all minimal combinations of metabolite uptakes and
secretions that support growth in the e_coli_core model, which can be
downloaded from BiGG [4]. This enumeration should finish within seconds.

API Documentation
=================

.. automodule:: mptool
   :members:
   :undoc-members:
   :show-inheritance:

References
==========

[1] O. Øyås, A. Theorell, and J. Stelling. "Scalable enumeration and sampling
of minimal metabolic pathways for organisms and communities". *bioRxiv* (2024).

[2] S. Klamt et al. "From elementary flux modes to elementary flux vectors:
Metabolic pathway analysis with arbitrary linear flux constraints". *PLOS
Computational Biology* 13.4 (2017).

[3] H.S. Song et al. "Sequential computation of elementary modes and minimal
cut sets in genome-scale metabolic networks using alternate integer linear
programming". *Bioinformatics* 33.15 (2017).

[4] Z.A. King et al. "BiGG Models: A platform for integrating, standardizing,
and sharing genome-scale models" *Nucleic Acids Research* 44.D1 (2016).
