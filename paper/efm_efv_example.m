clc; clear; close all;

% Load toy model
load models/toy.mat;

% Convert model to CNA project
cnap = CNAcobra2cna(toy);

% Enumerate EFMs
[efms,irrev,idx,ray,efv_with_zero] = CNAcomputeEFM(cnap);
efms

% Enumerate EFVs
constraints = struct;
constraints.lb = cnap.reacMin;
constraints.ub = cnap.reacMax;
[efvs,irrev,idx,ray,efv_with_zero] = CNAcomputeEFM(cnap,constraints);
efvs = unique((efvs ~= 0) .* sign(efvs),'rows');
efvs
