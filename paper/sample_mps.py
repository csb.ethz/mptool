#!/usr/bin/env python

import gurobipy as grb
from mpi4py import MPI
from mptool import *

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

# Choose total number of samples
n = 1e4

# Compute number of samples per process
max_mps = int(n / size)

# Load model and set parameters
model = grb.read('models/HS_BT_EC_FP_LL_LP_ST.mps')
model.setParam('OutputFlag', False)
model.setParam('OptimalityTol', 1e-9)
model.setParam('FeasibilityTol', 1e-9)

# Add rank to model name to ensure unique ID
model.modelName += '_' + str(rank)
model.update()

# Load bounds
bounds = load_bounds('data/HS_BT_EC_FP_LL_LP_ST_fva_bounds.csv')

# Sample MPs from subnetwork
mps, _, _ = find_mps(model, subset=set(), method='iterative', graph=False,
                     random=True, bounds=bounds, tighten=False, tol=1e-09,
                     inf=1000, threads=0, max_mps=max_mps, max_t=0,
                     verbose=False, export=False)

save_sets(mps, model.modelName + '_mps_' + str(rank) + '.csv')
