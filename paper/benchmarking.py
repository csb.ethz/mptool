#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import sys
import time
import gurobipy as grb
import numpy as np
import os

from mpgraph import load_list, enumerate_mps

# Command line arguments
model_path = sys.argv[1]
subset_path = sys.argv[2]
method = sys.argv[3]

# Load model and subset
model = grb.read(model_path)
subset = set(load_list(subset_path))

# Rename model
subset_id = subset_path.split('/')[-1].split('.')[0]
model.ModelName = subset_id + '_' + method
model.update()

# Set bounds
bounds = {v.varName: (v.lb, v.ub) for v in model.getVars()}

# Set parameters
graph = True if 'graph' in method else False
random = True if 'random' in method else False
method = method.split('_')[0]
inf = 1000
tol = 1e-9
threads = 1
max_mps = None
max_t = 3600
log = True
verbose = True

# Enumerate MPs and MCs
mps, mcs = enumerate_mps(model, subset=subset, method=method, graph=graph,
                         random=random, bounds=bounds, inf=inf, tol=tol,
                         threads=threads, max_t=max_t, max_mps=max_mps,
                         log=log, verbose=verbose)
